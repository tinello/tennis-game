package ar.com.tinello.katas.tennisgame

import assertk.assertThat
import assertk.assertions.hasMessage
import assertk.assertions.isEqualTo
import assertk.assertions.isFailure
import org.junit.Test



class GameTest {

    @Test
    fun `inicia un game love love`(){
        var game = Game("Gustavo", "Gabriel")

        val state = game.getState()
        assertThat(state).isEqualTo("Gustavo -> love|love <- Gabriel")
    }

    @Test
    fun `inicia un game fifteen love`(){
        var game = Game("Gustavo", "Gabriel")

        game.pointToPlayer1()
        val state = game.getState()

        assertThat(state).isEqualTo("Gustavo -> fifteen|love <- Gabriel")
    }

    @Test
    fun `inicia un game fifteen fifteen`(){
        var game = Game("Gustavo", "Gabriel")

        game.pointToPlayer1()
        game.pointToPlayer2()
        val state = game.getState()

        assertThat(state).isEqualTo("Gustavo -> fifteen|fifteen <- Gabriel")
    }

    @Test
    fun `inicia un game thirty fifteen`(){
        var game = Game("Gustavo", "Gabriel")

        game.pointToPlayer1()
        game.pointToPlayer2()
        game.pointToPlayer1()
        val state = game.getState()

        assertThat(state).isEqualTo("Gustavo -> thirty|fifteen <- Gabriel")
    }

    @Test
    fun `inicia un game thirty thirty`(){
        var game = Game("Gustavo", "Gabriel")

        game.pointToPlayer1()
        game.pointToPlayer2()
        game.pointToPlayer1()
        game.pointToPlayer2()
        val state = game.getState()

        assertThat(state).isEqualTo("Gustavo -> thirty|thirty <- Gabriel")
    }

    @Test
    fun `inicia un game forty thirty`(){
        var game = Game("Gustavo", "Gabriel")

        game.pointToPlayer1()
        game.pointToPlayer2()
        game.pointToPlayer1()
        game.pointToPlayer2()
        game.pointToPlayer1()
        val state = game.getState()

        assertThat(state).isEqualTo("Gustavo -> forty|thirty <- Gabriel")
    }

    @Test
    fun `inicia un game win loose`(){
        var game = Game("Gustavo", "Gabriel")

        game.pointToPlayer1()
        game.pointToPlayer2()
        game.pointToPlayer1()
        game.pointToPlayer2()
        game.pointToPlayer1()
        game.pointToPlayer1()
        val state = game.getState()

        assertThat(state).isEqualTo("Gustavo -> win|loose <- Gabriel")
    }

    @Test
    fun `inicia un game loose win`(){
        var game = Game("Gustavo", "Gabriel")

        game.pointToPlayer1()
        game.pointToPlayer2()
        game.pointToPlayer1()
        game.pointToPlayer2()
        game.pointToPlayer2()
        game.pointToPlayer2()
        val state = game.getState()

        assertThat(state).isEqualTo("Gustavo -> loose|win <- Gabriel")
    }

    @Test
    fun `inicia un game deuce deuce`(){
        var game = Game("Gustavo", "Gabriel")

        game.pointToPlayer1()
        game.pointToPlayer2()
        game.pointToPlayer1()
        game.pointToPlayer2()
        game.pointToPlayer1()
        game.pointToPlayer2()
        val state = game.getState()

        assertThat(state).isEqualTo("Gustavo -> deuce|deuce <- Gabriel")
    }

    @Test
    fun `inicia un game advantage -`(){
        var game = Game("Gustavo", "Gabriel")

        game.pointToPlayer1()
        game.pointToPlayer2()
        game.pointToPlayer1()
        game.pointToPlayer2()
        game.pointToPlayer1()
        game.pointToPlayer2()
        game.pointToPlayer1()
        val state = game.getState()

        assertThat(state).isEqualTo("Gustavo -> advantage|- <- Gabriel")
    }

    @Test
    fun `inicia un game - advantage`(){
        var game = Game("Gustavo", "Gabriel")

        game.pointToPlayer1()
        game.pointToPlayer2()
        game.pointToPlayer1()
        game.pointToPlayer2()
        game.pointToPlayer1()
        game.pointToPlayer2()
        game.pointToPlayer2()
        val state = game.getState()

        assertThat(state).isEqualTo("Gustavo -> -|advantage <- Gabriel")
    }

    @Test
    fun `inicia un game obtener end of game player1`(){
        var game = Game("Gustavo", "Gabriel")

        game.pointToPlayer1()
        game.pointToPlayer2()
        game.pointToPlayer1()
        game.pointToPlayer2()
        game.pointToPlayer1()
        game.pointToPlayer1()

        assertThat {
            game.pointToPlayer1()
        }.isFailure().hasMessage("End of game")
    }

    @Test
    fun `inicia un game obtener end of game player2`(){
        var game = Game("Gustavo", "Gabriel")

        game.pointToPlayer1()
        game.pointToPlayer2()
        game.pointToPlayer1()
        game.pointToPlayer2()
        game.pointToPlayer2()
        game.pointToPlayer2()
        game.pointToPlayer2()

        assertThat {
            game.pointToPlayer2()
        }.isFailure().hasMessage("End of game")
    }
}