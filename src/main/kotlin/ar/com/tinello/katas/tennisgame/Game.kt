package ar.com.tinello.katas.tennisgame

import kotlin.RuntimeException

class Game(val player1: String, val player2: String) {

    private val LOVE = 0;
    private val FIFTEEN = 1;
    private val THIRTY = 2;
    private val FORTY = 3;
    private val ADVANTAGE = 4;
    private val DEUCE = 5;
    private val WIN = 6;
    private val LOOSE = 7;
    private val NONE = 8;

    private val POINTS_DIFF_1 = 1;
    private val POINTS_DIFF_2 = 2;


    var player1points = 0
    var player2points = 0

    fun getState(): String {
        val points = getPlayersPointsName()
        return "$player1 -> $points <- $player2"
    }

    fun getPlayersPointsName(): String {

        var player1pontName = getPointName(player1points);
        var player2pontName = getPointName(player2points);

        if (player1points - player2points == POINTS_DIFF_1 && player2points > THIRTY){
            player1pontName = getPointName(ADVANTAGE);
            player2pontName = getPointName(NONE);
        }

        if (player2points - player1points == POINTS_DIFF_1 && player1points > THIRTY){
            player2pontName = getPointName(ADVANTAGE);
            player1pontName = getPointName(NONE);
        }

        if (player1points - player2points == POINTS_DIFF_2 && player2points >= THIRTY){
            player1pontName = getPointName(WIN);
            player2pontName = getPointName(LOOSE);
        }

        if (player2points - player1points == POINTS_DIFF_2 && player1points >= THIRTY){
            player2pontName = getPointName(WIN);
            player1pontName = getPointName(LOOSE);
        }

        if (player2points == player1points && player1points > THIRTY){
            player2pontName = getPointName(DEUCE);
            player1pontName = getPointName(DEUCE);
        }

        return "$player1pontName|$player2pontName"
    }

    fun getPointName(point: Int): String {
        return when (point) {
            LOVE -> "love"
            FIFTEEN -> "fifteen"
            THIRTY -> "thirty"
            FORTY -> "forty"
            ADVANTAGE -> "advantage"
            DEUCE -> "deuce"
            WIN -> "win"
            LOOSE -> "loose"
            NONE -> "-"
            else -> throw RuntimeException("Invalid Point")
        }
    }

    fun pointToPlayer1() {
        validateWin()
        this.player1points++
    }

    fun pointToPlayer2() {
        validateWin()
        this.player2points++
    }

    fun validateWin() {
        if (getPointName(player1points)=="win" || getPointName(player2points)=="win")
            throw RuntimeException("End of game")
    }

}
